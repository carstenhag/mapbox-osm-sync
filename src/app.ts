import { Dirent } from "fs";
const fs = require('fs');
const path = require('path');
const AWS = require('aws-sdk');
const PromisePool = require('promise-pool-executor');
const query_overpass = require('query-overpass');
const cron = require('node-cron');
const mbxUpload = require('@mapbox/mapbox-sdk/services/uploads');

const DATA_DIRECTORY_PATH = 'data'
const MAPBOX_ACCESS_TOKEN = process.env.MAPBOX_ACCESS_TOKEN;

if (MAPBOX_ACCESS_TOKEN === undefined || MAPBOX_ACCESS_TOKEN === '') {
  console.error('The MAPBOX_ACCESS_TOKEN environment variable is not set, but is required.');
  process.exit(1);
}

const uploadService = mbxUpload({ accessToken: MAPBOX_ACCESS_TOKEN })

const QUERY_SUFFIX = '.ql';
const GEOJSON_SUFFIX = '.geojson';

function queryOverpass(queryName: string): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    query_overpass(fs.readFileSync(path.join(DATA_DIRECTORY_PATH, queryName + QUERY_SUFFIX), 'utf8'), function (err: any, data: any) {
      if (err !== undefined) {
        reject(err);
        return;
      }
      fs.writeFileSync(path.join(DATA_DIRECTORY_PATH, queryName + GEOJSON_SUFFIX), JSON.stringify(data));
      console.log('Successfully saved dataset ' + queryName + ' to disk.');
      resolve();
    }, {flatProperties: true}) // Needed to get flat geojson properties -- configures osmtogeojson.
  });
}

function getCredentials(): Promise<any> {
  return new Promise(async (resolve: any, reject: any) => {
    try {
      let response = await uploadService.createUploadCredentials().send();
      resolve(response.body);
    } catch(err) {
      reject(err);
    }
  })
}

function putFileOnS3(credentials: any, query_name: string): Promise<any> {
  const s3 = new AWS.S3({
    accessKeyId: credentials.accessKeyId,
    secretAccessKey: credentials.secretAccessKey,
    sessionToken: credentials.sessionToken,
    region: 'us-east-1'
  });
  return s3.putObject({
    Bucket: credentials.bucket,
    Key: credentials.key,
    Body: fs.createReadStream(path.join(DATA_DIRECTORY_PATH, query_name + GEOJSON_SUFFIX))
  }).promise();
};

function createUpload(uploadURL: string, queryName: string): Promise<any> {
  return uploadService.createUpload({
    mapId: 'carstenhag.' + 'app-' + queryName,
    url: uploadURL,
    tilesetName: 'app-' + queryName
  }).send()
}

function listUploads(): Promise<any> {
  return uploadService.listUploads()
    .send()
}

class Query {
  constructor(public name: string) { }
}

function loadQueries(): Array<Query> {
  return fs.readdirSync(path.relative(".", DATA_DIRECTORY_PATH), {withFileTypes: true})
    .filter((item: Dirent) => !item.isDirectory())
    .filter((item: Dirent) => item.name.endsWith(QUERY_SUFFIX))
    .map((item: Dirent) => new Query(item.name.substring(0, item.name.length - QUERY_SUFFIX.length)))
}

async function main() {
  await startQueries();
  console.log("Scheduling queries to run at 22:00");
  cron.schedule("0 0 22 * * *", () => {
    console.log("Starting scheduled queries \n")
    startQueries();
  })
}

async function startQueries() {
  const queries = loadQueries();

  const promisePool = new PromisePool.PromisePoolExecutor({
    concurrencyLimit: 1 // Default Overpass server does not allow multiple queries at the same time
  })

  await promisePool.addEachTask({
    data: queries,
    generator: async (query: Query) => {
      // 1. Query Overpass and save response file.
      console.log("Querying Overpass with query " + query.name);
      await queryOverpass(query.name).catch(err => {
        console.error("Error while querying Overpass for " + query.name, {err})
      });

      // 2. Check whether the written response file exists
      if (!fs.existsSync(path.join(DATA_DIRECTORY_PATH, query.name + GEOJSON_SUFFIX))) {
        console.error("Can't upload file " + query.name + " as it does not exist in the data directory.")
        return;
      }

      // 3. Upload tileset to Mapbox
      let credentials = await getCredentials();
      await putFileOnS3(credentials, query.name);
      console.log("Uploaded the file " + query.name + " to the staging S3 bucket."  )
      await createUpload(credentials.url, query.name);

      // 4. Wait specified time in order not to overload the Overpass API.
      console.log('Waiting 300 seconds in order not to overload the API.');
      sleep(300);
    }
  }).promise()
}


// https://github.com/erikdubbelboer/node-sleep
function msleep(n: number) {
  Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n);
}
function sleep(n: number) {
  msleep(n*1000);
}

main()